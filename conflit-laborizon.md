# Traversée des tensions et conflits à Labôrizon
[toc] 

Materiel inspirant : 
- [Fiche de Virginie Deleu](https://framavox.org/rails/active_storage/disk/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdDVG9JYTJWNVNTSWhObmRqWkdFM1lYZHNjbnBwZUdRMWRXZHpiWEJ6WWpNeU5qSTBjZ1k2QmtWVU9oQmthWE53YjNOcGRHbHZia2tpU1dsdWJHbHVaVHNnWm1sc1pXNWhiV1U5SWxSeWFYQjBhWEYxWlhBeUxuQmtaaUk3SUdacGJHVnVZVzFsS2oxVlZFWXRPQ2NuVkhKcGNIUnBjWFZsY0RJdWNHUm1CanNHVkRvUlkyOXVkR1Z1ZEY5MGVYQmxTU0lVWVhCd2JHbGpZWFJwYjI0dmNHUm1CanNHVkRvUmMyVnlkbWxqWlY5dVlXMWxPZ3BzYjJOaGJBPT0iLCJleHAiOiIyMDI0LTAyLTE1VDE4OjE4OjMyLjY3MFoiLCJwdXIiOiJibG9iX2tleSJ9fQ==--cbec95eb042282260e32627319f805e5d4e0923a/Triptiquep2.pdf)
- [Padlet de Virginie Deleu](https://padlet.com/ice3/violences-scolaires-omm3bvf081sfjusx)

----

## Escalade du plus rapide/simple au plus énergivore
Il est possible de sauter une/des étapes, éventuellement de revenir en arrière, mais pas de réclamer que des étapes anterieurs soient effectuées si une des personnes concernée se sens plus loins dans l'escalade.

1. **intra-personnel :** suis-je en mesure de résoudre la situation de manière satisfaisante moi-même ? + s'offrir de l'auto-empathie
2. **intra avec soutien :** demander de l'écoute (entraide à la présence, à la clarté, à la puissance) pour être en mesure de répondre au point précédent ou de passer au suivant.
3. **inter-personnel :** Exprimer une demande à la/les personnes conserné⋅e, idéalement en clarifiant avec une approche [OSBD/CNV](https://apprentie-girafe.com/osbd-observation-sentiment-besoin-demande/) (sans en faire un passage obligé)
4. **inter avec soutien :** demander/bénéficier d'une tiers personne présente pour faciliter la communication de manière informelle (reformulation si ça ramme, proposition de pause...)
5. **systémique fuidiste :** Apporter la tension en réunion pour en parler collectivement et voir ce qui en émerge
6. **inter-personnel structuré :** Demander une médiation formelle (si le conflit semble essentiellement inter-personnel) faciliter par un⋅e autre habitant⋅e (surtout si l'étape 4 à été sauté/traversée dans de mauvaise conditions)
7. **systémique structuré :** Appeler un cercle, si possible facilité en interne, sinon faire appels aux personnes ressources disponible.
8. **systémique panique/désespoir : Pour sortir des ornières du désespoir :** Utiliser la procédure d'exclusion si ça semble pertinant.
9. **Dernier filet de sécurité** : Faire appel à la justice d'état

## Comment se saisir de chaque étapes ?

La recommandation est d'envisager chaque étapes, d'essayer à minima les premières (intra-personnelle) pour y voir plus clair sur les étapes suivantes les plus pertinantes pour la situation si l'intra-personnel n'a pas suffit.

Si vous avez un enjeu de confidentialité, les approches systémiques risquent d'être plus difficile à utiliser.

### 1 Comment me soutenir moi-même ?
1. Prévention : 
    - apprendre l'auto-empathie
    - écrire son mode d'emploi pour mieux connaitre son propre fonctionnement et pouvoir le communiquer (dont fiche sos)
    - ritualiser des temps pour pratiquer l'auto-empathie
2. Quand c'est chaud : 
    - respirer
    - S'éloigner de la situation
    - "Hurler"/exprimer ses besoins en souffrance
    - Aller faire de l'exercice physique
3. Après coup : 
    - Prendre un temps pour s'offrir de l'auto-empathie
    - Prendre acte de la situation : notament tenter de différentier la réalité conscenssuelle (les faits) de la réalité non consenssuelle (le reste : ses récits, interprétations, projections...).
    - Chercher son propre pouvoir pour faire évoluer la situation.
### 2 Comment demander du soutien ?
1. prévention : constituer son système de soutien.
    - identifier des personnes exterieures avec qui faire des écoutes régulières réciproques, que ce soit par hygiène émotionnelle ou pour construire la confiance que des écoute en urgence seront soutenante.
    - convenir d'une manière de demander de l'écoute en urgence avec qui vous souhaitez.
    - être habitant⋅e⋅s de Labôrizon implique d'être prêt⋅e à recevoir des demandes d'écoute des autres habitant⋅e⋅s (mais pas forcément d'y répondre favorablement)
    - si vous voyez venir un moment chaud, préchauffez votre système de soutien : demander préventivement qui serait disponible pour vous écouter sur la periode où vous pensez en avoir besoin.
3. Quand c'est chaud : 
    - solicitez l'écoute des personnes qui vous semble disponible, idéalement en vérifiant explicitement leur disponibilité.
    - appellez les personnes de votre système de soutien
    - envoyez par sms "SOS {sujet en 3 mots max} {degré d'urgence sous forme de temps de réaction max souhaité}" aux membre de votre système de soutien. Exemple : "SOS enguelade avec {un⋅e habitant⋅e} <30min"
5. Après coup : 
    - demander aux personnes qui nous inspirent, dans notre système de soutien, un rendez-vous pour une écoute (convenir éventuellement de la durée)

### 3 Comment s'assurer de la transmission d'un message/demande ?
- En présentiel : 
    1. Demander "Es-tu disponible pour m'écouter au sujet d'une tension te concernant." (si non, demander "Quand peux tu te rendre disponible pour ça ?", si la réponse est insatisfaisante, escalader.)
    2. (si oui) Exprimer sa tension, si possible en mentionnant un fait précis l'illustrant. Eventuellement proposer une solution (en étant prêt⋅e à ce qu'elle soit questionnée/refusée/ajustée).
    3. Demander "Qu'as-tu compris ?"
    4. Confirmer ou ajuster.
- En distanciel & asynchrone : 
    1. Exprimer sur le matrix de Labôrizon, dans Pôle tensions, Mode chacal ou Mode girafe, sa tension.
    2. Expliciter à qui elle s'adresse et lui demander "Qu'as-tu compris ?".
    3. Ne considérer le message lu qu'après que la personne est explicitement dit "Lu", commenté d'un simley le message ou reformulé ou réagi.
    4. Sans "Lu" explicite après 24h, contacter la personne destinatrice par sms pour l'informer qu'un message la concernant se trouve dans la catégorie du matrix où on l'as mis.
    5. Réitérer par sms ou email après 1 semaine sans réponse (si exprimé en Mode girafe. Si exprimé en mode Chacal, demander du soutien pour l'exprimer en Mode girafe avant de relancer.)
    6. Ne pas considérer le message comme lu du fait d'un accusé réception provenant du logiciel et non d'une action dédiée dula destinataire.

### 5 Comment apporter une tension à l'ordre du jour d'une réunion ?
Comme n'importe quel autre point, donner un mot clef au moment du recueil des points du cercle de triage pour que la parôle vous soit donné pour exposer votre point.

Idéalement, poser vous la question de ce que vous attendez du groupe en ammenant le point en réunion (exemple : une solution, d'être entendu⋅e, de connaitre le point de vu des autres...). Si vous ne savez pas, la question vous sera probablement posé durant la réunion.

### 6 Comment demander une médiation ?
Demander à qui vous voulez de médier. Si la médiataire et l'autre/les autres personnes concernées acceptent, laissez la médiataire convenir du processus et des rendez-vous nécessaire avec les concerné⋅e⋅s.

Si la médiation implique une contrepartie/rémunération référez-vous au [7bis Comment faire appel à des personnes ressources exterieures ?](#7bis-Comment-faire-appel-à-des personnes-ressources-exterieures-?) avant de valider le processus.

### 7 Comment appeler un cercle ?

1. Listez les personnes que vous imaginez pouvoir faciliter le cercle (si vous n'avez pas d'idées, listez les personnes que vous ne voudriez surtout pas avoir comme facilitataire, les autres habitant⋅e⋅s trouverons qui d'autre peut faciliter)
2. Listez les personnes dont la présence serait nécessaire pour faire évoluer la situation selon vous.
3. En fonction de l'urgence : 
    - Demandez en réunion de coloc qui est prêt à faciliter un cercle ou à trouver quelqu'un⋅e pour le faire (en indiquant vos préférences du 1.). L'une d'entre elle (nomons-là "proto-fa") prend la charge d'arriver à un⋅e facilitataire pertinante pour touz.
    - Vous pouvez tenter d'accélérer les choses  en indiquant "Demande de Cercle par {votre nom}" sur le barromètre des tensions de l'entrée. La première personne qui est prêt à prendre le rôle "proto-fa" viens vous demander les listes des deux premiers points pour effectuer les étapes suivantes, et indique sur le tableau qu'iel s'occupe de trouver lae facilitataire.
4. Proto-fa demande aux personnes que vous avez listez en (2.) d'indiquer au cours de la réunion ou sous 24h si les facilitataires que vous avez listé leur conviennent également.
5. En fonction des cas :
    - Si plusieurs personnes font conssensus, proto-fa leur demande leur disponibilité et conditions et tranche à son appréciation (ou en concertations avec les concerné⋅e⋅s si ça ne retarde pas de plus de 24h). Une autre personne du pool sera bienvenu en soutien à la facilitation.
    - Si personne ne fait conssensus, proto-fa prend la mission de chercher des personnes exterieures sous 7jours (avec tout le soutien disponible). Si les personnes proposé ne conviennent pas, une personne est tirée au sort parmis toutes celles envisagée par les concerné⋅e⋅s. Si elle refuse, une autre et tiré au sort... Si personne n'est trouvé sous 1 mois, l'échec du cercle est actée en réunion de coloc et l'ordre du jour est réservé à trouver une issue (qu'il s'agisse d'arbitrage d'exclusion, de médiation, de prendre des vacances...).
6. Le détails des étapes du cercle sera présenté par la personne qui facilite (généralement avants cercles individuels, avant cercle du facilitatire, cercle(en 3 phases : conséquence au présent, situation passé, choix futurs), après-cercle(s) )

### 7bis Comment faire appel à des personnes ressources exterieures ?
*(ou à des habitant⋅e⋅s qui souhaiteraient une contrepartie / rémunération)*

1. identifez une personne dont vous souhaiteriez le soutien  et demandez lui ses conditions.
2. Si ce soutien implique d'autre personnes, demandez leur un accord de principe / si la personne solicité leur inspire confiance, indépendament de ses conditions/contrepartie.
3. Demander en réunion si Labôrizon prend en charge les conditions (ou un plafond de prise en charge consenti par le groupe)¹
4. Continuez comme pour les soutien/entraide/médiation interne/bénévole.

¹ En cas de refus de prendre en charge par Labôrizon, à vous de trouver des conditions qui convienne à la personne solicitée et aux autres concerné⋅e⋅s (si c'est vous qui payez, attention aux risque de partialité lié à l'origine de la rémunération).

### 8 Comment déclencher un arbitrage pour exclusion ?
Chaque habitant⋅e⋅s peut déclencher un arbitrage pour exclusion comme suit : 

1. Informer toustes les habitant⋅e⋅s et autres concernées le cas échéant : 
    - Si une réunion est proche et que tout le monde y est, l'annonce du processus peut se faire en réunion.
    - S'il y a des absents en réunion ou que la réunion est dans trop longtemps pour votre gout, contactez les absents à minima par email et sms pour leur signaler et indiquez le sur le tableau de l'entrée ainsi que sur le matrix de Labôrizon.
2. Quel que soit le cas au point précédent, fournissez un framadate (ou équivalent) commençant 7 jours après votre déclenchement et proposant au moins 30 jours pour qu'une date de réunion dédié puisse avoir lieu. Profitez-en pour demander qui est prêt à faciliter le temps de discussion, et qui est prêt à organiser le vote (si personne, c'est probablement sur vous que ça retombera).
3. La suite du processus est décrite dans la section [Arbitrage exclusion](#Arbitrage-exclusion).

### 9 Comment entamer une procédure juridique ?
La situation relève tel du pouvoir d'un consiliateur de justice ? ou est elle opposable juridiquement ?

- Conseil juridique : Les points justice, les maison de justice et du droit propose des permanance gratuite.
- En ligne : https://www.masecurite.interieur.gouv.fr/fr/demarches-en-ligne/visioplainte-depot-de-plainte-visioconference ou https://www.masecurite.interieur.gouv.fr/fr/demarches-en-ligne/pre-plainte-en-ligne ou https://qualiplainte.fr/deposer-une-plainte-en-ligne/
- En gendarmerie / commiçariat de police

## Comment soutenir dans chaque étape ?

### Répondre à une demande d'écoute
- Si vous n'êtes pas disponible (agenda, ressenti...), dès que vous apprenez la demande, répondez "pas dispo".
- Si vous n'êtes pas disponible pour écouter, mais pour aider à trouver quelqu'un d'autre oui, faite une proposition. Exemple : "je suis HS, est-ce que {habitant⋅e} serai dispo ? sur le groupe de soutien {nom/url canal de messagerie dédiée}"
- Si vous vous sentez disponible, dite le, éventuellement accompagné de vos préalable et limites. Exemple : "ok dans 5 minutes, pour 30minutes max, dans ma chambre" ou "ok de 15h à 16h par téléphone, il se peut que j'ai à m'intérompre de temps en temps, j'aurai la garde de mon enfant."

### Styles d'écoutes

(BIENTOT)

### Style de facilitation et médiation
+ conciliation + arbitrage + jugement
(BIENTOT)

### Arbitrage exclusion
(ou légitimer le fait de prononcer une exigence de sortie au nom du collectif)
#### Contexte : 
Exclure n'est jamais satisfaisant pour tout le monde. Ce processus à pour seule ambition de permettre à la majeure partie du groupe de survivre à une situation qui draine plus de ressources que celles disponibles. Le groupe se fragilise et se met en danger en s'enlisant davantage dans une situation qui le dépasse, d'où la nécéssiter d'arbitrer une exclusion pour réduire la complexité de la situation à gérer et de revenir dans ce qui est gérable avec les ressources du groupe.

#### Modalités : 
*~~Toute opposition à une exclusion doit être accompagné d'une proposition alternative (exclusion de quelqu'un⋅e d'autre, implication personnelle pour trouver une personne ressource externe pour accompagner la situation...)~~*

Quand un arbitrage pour exclusion est déclenché, chaque habitant⋅e est invité à explorer la situation sous plusieurs angles idividuellement, puis d'en discuter en réunion avant de trancher par un vote, puis de déterminer les modalités (délais essentiellement) de départ dula concerné⋅e.

##### 1. réflexion individuelle & discussion
Durant cette phase, chaque habitant⋅e est invité⋅e à se questionner pour chaque habitant⋅e :
Quelles seraient les conséquences de l'exclusion de Labôrizon pour chaque personne ?
Pour nourrir ce questionnement, les angles suivants sont proposé (non exhaustifs) : 

- Faciliter la reconstruction du collectif : (principalement pour déterminer qui exclure)
    - qui sont les personnes incompatible entre elle avec les ressources actuelles ?
    - quelles sont les combinaison de personnes les plus stable/ stabilisante ?
    - qui sont les plus nécessaire au fonctionnement du collectif ? (contribution au quotidien, stabilité symbolique, *position de pouvoir*...)
    - qui dégrade la qualité du lieu ou du vivre ensemble, cause primaire ou secondaire ?
    - qui met le plus en danger le collectif / draine ses ressources de par sa situation (autant que possible de manière factuelle, dépassionnée, sans jugement sur la personne) (*position de pouvoir encore*) *garder en tête qu'une personne qui critique/remet en cause puise des ressources, ce qui est fatiguant pour le groupe et peut le mettre en danger s'il manque de ressource, mais amorce également les changement s'il y a les ressources pour faire de ces critiques des améliorations (qui peuvent également sauver le collectif)*
    - y a t'il un effet soupape/fusible/bouc émissaire dans la dynamique de groupe, qui contribue à le produire/maintenir, qui en fait les frais ?
    - du départ de qui les réscapé⋅e⋅s peuvent-iels se remetre émotionnellement (et materiellement) le plus facilement ?
- Minimiser le poid de l'exclusion : (autant pour déterminer qui exclure que pour déterminer les modalités/délais de départ)
    - qui peut retrouver un logement le plus facilement ? (materiellement)
    - qui peut le plus facilement reconstituer un environnement lui convenant au moins aussi bien que Labôrizon ?

Après ce temps de réflexion individuel, un temps de partage égalitaire en temps de parôle max par orateurice et par personne concerné⋅e à lieu (durée à convenir ensemble, le plus petit temps de disponibilité est retenu avec pour minimum : 1min/orateurice/concerné⋅e)

##### 2. Le vote
Chaque habitant⋅e (avec un quorum à 75% arrondi à l'inferieur) va se positionner par rapport à l'exclusion de chaque autre habitant⋅e (et le cas échéant celle d'invité⋅e⋅s).

Le vote aura lieu à bultin secret sauf à ce que tout le monde souhaite se confronter au vote nominal de tout le monde (et assumer son propre vote auprès des autres)

~~Il sera également demandé de se position soi-même vis à vis de soi-même, de manière indicative/non comptabilisé sauf pour trancher en cas d'égalité ou de demande de vote à bultin secret.~~



Pour chaque habitant⋅e, les positionnement peuvent être : 
- -2 à ne surtout pas exclure
- -1 défavorable à l'exclusion
- 0 neutre
- 1 exclusion envisageable
- 2 exclusion nécessaire

Voici un outil permettant ce mode de scrutin : https://jugementmajoritaire.net/ ou (autre geekage d'outil si besoin)

Ne sera exclue que la personne finissant avec le plus gros score et seulement si celui-ci est superieur à 0.

En cas d'égalité de score, la personne ayant le résultats le plus clivant (notes extrèmes) est exclue en priorité.
En cas d'égalité malgré tout, un 2nd tour entre les personnes à égalité aura lieu.
Si l'égalité persiste, l'exclusion sera tiré au hasard entre les personnes à égalité.

Si plusieurs personnes ont des scores superieur à 0, un second arbitrage 1 mois plus tard sera effectué pour déterminer s'il y a besoin d'une autre exclusion ou non. Et ainsi de suite tant qu'il y a des scores superieur à 0.

Exclure quelqu'un⋅e lui retire le fait d'être implicitement bienvenue à Labôrizon. Iel peut néanmoins être invité⋅e par un⋅e habitant⋅e après avoir eu l'accord de l'intégralité des autres habitant⋅e⋅s avec des conditions à leur discretions notamment une durée pré-déterminée.

Lorsqu'un⋅e personne est exclue, la décision est actée, elle ne pourra être requestionnée qu'à minima 1 an après le départ effectif de la personne. Reste à statuer des modalité de départ / délais d'application.

##### 3. Délais et autres modalité de départ
Pour un⋅e invité⋅e, la prise d'effet de l'exclusion peut être immédiate. Pour un⋅e habitant⋅e, un mois minimum est laissé après que l'exclusion ai été pronnoncée.

Les modalités de départ peuvent être : 
- le délais avant que la personne soient poussée dehors par tous moyens à disposition de Labôrizon si elle n'est pas partie plus tôt.
- des modalité financières (caution, remboursement de dettes, loyer modifié...)
- de la mise à disposition materiel (véhicule et/ou bras pour le déménagement)
- du soutien à trouver un autre logement
- ...

En l'absence d'accord préalablement établie ou établie à l'issue d'une discussion sur les modalités de départ, le délais minimum d'1 mois s'applique pour les habitant⋅e⋅s (immédiat pour les autres) et sans autres compensations ou soutiens.

Si la personne exclue est sur le baïl, le tier de confiance à qui a été remis les préavis de départ signés de chaque habitant⋅e sera contacté par une des personnes restante. Celle-ci lui demandera de transmettre à l'agence le préavi de départ de la personne exclue. Le tier de confiance est tenu de vérifier auprès de la personne exclue ou si elle ne confirme pas, auprès de 75% arrondi à l'inferieur des habitant⋅e⋅s qu'il y a bien un accord d'exclusion avant de transmettre le préavi à l'agence.


### Soutien administratif et légal
Faire appel à un⋅e amie
Se référer au point [9 Comment entamer une procédure juridique ?](9-Comment-entamer-une-procédure-juridique-?)

## FAQ
- **Qui peut se saisir de ce processus d'escalade :** *Toute personne le souhaitant, mais seule les habitant⋅e⋅s de Labôrizon peuvent exiger que leur point soit traité à l'ordre du jour d'une réunion, déclencher un cercle ou un arbitrage. Les autres peuvent demander mais pas exiger. Si aucun⋅e habitant⋅e n'a envie de prioriser votre demande, elle pourra rester sans suite, mais vous pouvez tout de même essayé, il y a de forte chance que nous ayons envie de prendre en compte votre demande si cela n'est pas trop énergivore.*
- **J'ai l'impression que tout le monde s'en fou, de ne pas être soutenu et d'être seul⋅e avec ma tension :** *C'est une tension en soi qui peut être traitée selon le même processus d'escalade.*
- **Comment remettre en cause et faire évoluer tout ça :** *En considérant les changements souhaités comme des tensions et en suivant le même processus (notament l'étape 5 est suceptible de faire émerger des groupe de travail à ce sujet)... Ou en ruant dans les brancart jusqu'a ce que les autres prennent en compte la guerria en cours.*
- **Je préfère régler mes problèmes autrement :** Libre à vous de proposer d'autre approches que celles listées dans ce document. Le risque de raté est plus fort si cela implique des personnes qui ne sont pas familières avec votre approche, mais si elles sont d'accord pour essayer avec vous, faites donc. Sans leur accords, cela risque de complexifier en rajoutant des tensions, il y a surement des situations ou ça se justifie, mais pesez votre choix avant de foncer.

