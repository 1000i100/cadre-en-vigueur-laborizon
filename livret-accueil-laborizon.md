---

---

# Livret d'accueil Labôrizon

Les questions qu'on se pose en arrivant, les infos utiles à transmettre, comment faire des ajutements, co-construire...

<!-- Version 2 alpha [TOC:2:5] https://hedgedoc.dev/n/sf6pvy1byedvhtwjvc8m6ydgec -->

[TOC]

## Pour touz (habitant⋅e⋅s, invité⋅e⋅s...)


## Spécifique organisataire d'événement
Que vous soyez habitant⋅e⋅s ou non, si vous organisez un événement à Labôrizon, cette partie est pour vous.

### Privilège habitant⋅e⋅s
- Vous êtes bienvenue à participer gratuitement sauf accord spécifique contraire.
- Vous pouvez poser un véto sur l'accueil d'un événement (il poura être contesté par les autres habitant⋅e⋅s, cf traversée de conflit)
- Vous pouvez choisir d'accueillir des événements d'une demi journée maximum 1 à 2 fois par mois max, qui ne nécessite que l'usage d'une pièce (max 15 personnes) sans accord préalable des autres habitant⋅e⋅s, en les en informant si possible (ou à défaut en étant en mesure d'accueillir dans son espace privé ce qui aurait eu lieu dans un espace commun sinon). Ce pouvoir n'empèche pas les autres habitant⋅e⋅s de poser un véto une fois informé⋅e⋅s.

## Spécifique habitant⋅e⋅s

### Accès (boite à clefs, wifi...)
- fermer à clef la nuit + rangement de clef sur l'interphone
- boite à clef

- Le code wifi
    Il y a 3 wifi à Labôrizon :
    - Plug & Surf, en libre accès sans mot de passe.
    - mdp:password, au mode de passe explicite (password)
    - Local+Internet, donnant également accès au NAS, à l'imprimante et peut-être d'autres choses (caméra) dont le mdp est : bienvenuecheznous
    - Lien vers le matrix?
### Fonctionnement des espaces
#### Entrée & couloir
- Gestions des chaussures (et prêt de chaussons)

#### Cuisine
- agencement cuisine
- Les étagères perso dans le frigo et les placards
- Si on choisit d'avoir une bassine dans l'évier : comment on s'en sert ?
#### Toilettes
Voir [Toilettes sèches](#Toilettes-sèches)
#### Salle de bain
- espace/organisation rangement SdB
  (bientôt) Un meuble de douche dispose d'un étage affaire perso, les autres affaires visibles dans la pièce sont en libre accès.
  Dans les placards, un espace de stock commun est présent en bas à gauche, un espace pharamcie juste au dessus, les autres espace sont nominatifs avec étiquettes.
#### Séjour
- Fonctionnement de la bibliothèque : cf [Bibliothèque](#Bibliothèque)
### Bibliothèque
- organisation bibliothèque
    Le rangement est thématique selon les étiquettes de casier.
    Prendre soin des ouvrages est important pour nous, si vous envisagez de déteriorer ou simplement de personnaliser un ouvrage que vous n'avez pas acheté, merci de demander à la personne qui l'a acheté, ou d'en acheter un autre exemplaire. En cas de dégradation accidentelle, perte ou vol, merci si possible de remplacer l'ouvrage, sinon à minima, d'en discuter avec l'acheteurice pour trouver une solution qui convienne aux concerné⋅e⋅s (cf traversée de conflit si besoin).
    Il est possible de mettre son nom éventuellement son numéro de téléphone en début de livre pour savoir à qui demander en cas de demande d'emprunt, de départ d'habitant⋅e⋅s, ou pour faciliter, pour qui emprunte, le fait de savoir à qui rendre les livres empruntés.
    Il est recommandé d'indiquer à qui et jusqu'à quel date quel livre à été prêté sur [cette page](https://demo.hedgedoc.org/s/_sTIIC_mF) ou sur le carnet papier (bientôt) posé en haut de la bibliothèque.

### En vrac...
- Comment s'incarnent les valeurs de Labôrizon

- monde exterieur à proximité/bon plans (boulangerie, tram, boutique vélo, resto...)

- les vélos
- checker du confort de chacun⋅e niveau bruit

- le NAS
- fonctionnement linge
  Le lave linge se trouve coté garage/bureau/atelier, dans la buanderie, au fond à gauche.
  Il y a un étendoir généralement dans le couloir.
  des fils à linge dans le séjour sur les barreaux de la mezzanine, un mini étendoir complémentaire dans la SdB, et (bientôt) un fils à linge dans le jardin.
  La bassine grise sert à transporter le linge propre
  Certaines personnes ont du linge (de lit ou non) perso et le gèrent en autonomie.
  Pour le linge commun (de maison notamment, literie, torchons, serviettes, peut inclure les serviette de bain), il y a une grande panière dans le couloir pour le sale, chacun⋅e est libre de le laver, et les journée ménage peuvent être l'occasion de le faire si ça n'a pas été fait.


### Présentation et modes d'emploi des habitant⋅e⋅s
(y compris les non humains)
- les triggers des personnes ici (anciennes et nouvelles)

### Gestion des déchets
#### Compost
Tout les déchets organiques vont au compost en priorité : 
- épluchures de légume (ou légumes abimés)
- pelure de fruit (y compris agrume)
-> Pour les fruits et légumes, il est possible de donner une partie des épluchures au lapin suivant son régime alimentaire
- sachets de thé sans plastique
- papiers, cartons non vernis/ non-plastifiés, mouchoirs sales (exception : on garde les cartons de pizza pour les toilettes sèches)
- os, restes de viande ou de produit laitier
- nourriture avariée en général.

Un bol métalique "mini compost d'interieur" est disponible près de l'évier sur la gauche. Dès qu'il est plein, videz-le dans le bac à compost exterieur.

Les petits seaux rectangulaires vert à couvercle ont un usage individuel : par défaut on ne s'en sert pas. Si vous commencez à en remplir un, vous êtes responsable de le vider et le rincer dès que vous avez fini de vous en servir (et avant qu'il ne commence à puer ou grouiller).

Le bac à compost exterieur, au fond à droit du jardin, bac plastigue vert est l'endroit ou vider les autres composte allimentaire. Après chaque vidage, autant que possible, recouvrez de feuilles ce que vous venez d'y mettre.
Une fois par trimeste, retourner le composte est une bonne idée.


#### Toilettes sèches
On a des toilettes sèches dans la maison (et pas de toilettes à eau).
Le fonctionnement (qui est écrit sur le mur à gauche des toilettes quand on est assis) : 
- S'asseoir (quelle que soit son anatomie)
- Recouvrir de sciure / copeaux de bois avec la petite pelle après tout usage. La sciure est à côté des toilettes soit dans le petit réservoir derrière
- On peut jeter les tampons ou contenus de cup menstruelles dans le bac des toilettes sèches (mais pas les serviettes jetables)

Quand le bac est plein (à peu près 10 cm en dessous du niveau de la lunette, ou dès qu'on estime ne plus être à l'aise avec le niveau de remplissage du bac) : 
- La planche de bois où est vissée la lunette se relève et le pivot se coince en appuyant sur les charnières en métal pour éviter que la planche ne retombe
- On soulève le bac bleu jusqu'à l'enlever du compartiment en bois, ou jusqu'à pouvoir le poser sur le rebord
- On referme les deux pans du bac bleu
- On dépose le bac dehors 
    - idéalement derrière le mur de la buanderie, pour que ce soit plus facile à vider ensuite)
    - a minima sur la terrasse près de la porte

Remettre un bac : 
- Les bacs vides sont dehors à côté de la porte, ou dans le jardin près du compost
- On met un carton de pizza au fond du bac, plié de sorte que le fond du bac soit couvert et éventuellement les côtés en partie
- On ouvre le battant en bois sur lequel est vissé la lunette, en coinçant la fermeture
- On dépose le bac déja ouvert : 
    - au fond du support en bois, par dessus le morceau de plastique qui surélève 
    - avec les aillettes / battant sur chaque côté gauche et droite
- On s'assure que le bac soit bien collé à l'avant du compartiment en bois (que le bord du bac soit le plus près possible de la planche avant qui a un triangle)
- On referme la planche des toilettes

Remettre de la sciure : 
- Les sacs de sciure en stock sont dans la buanderie (ou stockés dans le vélo cargo)
- Tous les trimestres, un.e coloc va chercher de la sciure à la scierie La Planche, avec le vélo cargo (penser à emmener les sacs vides pour les remplir sur place)

Gestion des toilettes sèches : 
- C'est Julia qui vide les bacs pleins
- La prévenir dès qu'il y a 4 bacs remplis (on en a 9 au total)
- Faire circuler les infos sur le stock de sciure restant
- Si à un moment il y a un problème de gestion des toilettes sèches (plus de bacs vides, plus de sciure, odeur persistante, manque d'énergie collective à l'entretien et au maintien du fonctionnement des toilettes sèches) : on peut récuperer des toilettes à eau (contacter la Fumainerie) et trouver quelqu'un pour aider à les réinstaller


#### Litière animaux

#### Poubelles
- **Compost** : cf [Compost](#Compost)
- **Recyclage** : la très grand majorité des déchets vas au recyclage, une poubelle éponyme se situe dans la cuisine. Quand elle est pleine, videz-là dans la poubelle exterieur verte. Quand cette dernière est proche d'être pleine, mettez là dans la rue (au bout de l'allée, en la collant contre les murs pour permettre le passage sur le trottoir et ne pas gêner le passage des voitures dans l'allée), elle sera vidé par les service municipaux dans la **nuit du mardi au mercredi**. Si vous la croisez dans la rue, rentrez-là dans le jardin.
- **Déchets ultime** : une poubelle éponyme est présente dans la cuisine, elle se vide dans la poubelle grise dehors, que vous pouvez mettre dans la rue le **lundi soir ou jeudi soir** pour que les services municipaux la vide (voir Recyclage pour plus de détaille).
- **Verre** : une poubelle éponyme est présente dans la cuisine, elle se vide dans une des bennes à verres du quartier après avoir enlevé les couvercles des bocaux pour les mettres au recyclage. Les bennes les plus proches se trouvent : 
    - en sortant de l'impasse, continuez en face puis à gauche jusqu'au voies du tram, là, juste sur votre gauche, il y a une benne enterrée.
    - en sortant de l'impasse, continuez en face puis à droite, puis à gauche jusqu'à la maison de quartier/centre social rue Pierre et Marie Curie sur la droite avec 2 bennes bleue qui donnent sur la rue.

### Ménage
- Post-it pédagogique : 
    Des post-it en forme de coeur avec un smiley "yeux plissés" sont stocké dans la cuisine sous le placard vitré, proche de l'escalier. Toute personne en tension avec du désordre peut en prendre un et le place là où elle aimerait que du rangement/nétoyage ait lieu. L'intention est d'aider la/les personnes qui auraient généré ce désordre à prendre conscience de leur impact sur le lieu et à le prendre en charge. Il est également possible de prendre en photo les scènes de désordre avec post-it pour les poster sur le canal matrix Pole concrêt > vie quotidienne avant de nettoyer, pour que la prise de conscience puisse se faire, sans que cela implique de conserver le désordre.
- ménage hebdomadaire
- journée ménage mensuelle
    Une fois par mois environ, touz les habitant⋅e⋅s sont invité.e.s à se réunir 3h pour contribuer dans la matière à perfectionner l'état de la maison. La présence n'est pas strictement obligatoire, mais l'absence risque d'être rapidement source de conflit si elle n'est pas proportionnée à la présence sur les lieux.
- zèle ménage : 
    Toute contribution au ménage est bienvenue que vous soyez habitant⋅e ou non.
### Finances
- partage des charges
- partage du loyer
- caution
- pot commun
- pot libre service
- politique alimentaire commune / courses mutualisées

### Gouvernance
#### Mode de prise de décisions selon les sujets

#### Mode de traitement des conflits (traversée et arbitrage)
  Voir : https://demo.hedgedoc.org/gtRMrYQFQnqTZ7Sm3zSfCw#
#### Processus d'intégration
1. candiature / recrutement
    - Envoyer un email de candidateure à contact@laborizon.org (bientôt)
    - ou en parler à un⋅e habitant⋅e⋅s et laisser cette personne relayer votre demande.
3. rencontre / découverte mutuelle
    Si possible en présence, à défaut en visio serons exploré :
    - la découverte des lieux (ses valeurs aussi bien que sont agencement spacial)
    - les grandes lignes de fonctionnement de Labôrizon
    - les trait de personnalité et besoin spécifique de la personne candidate et des habitant⋅e⋅s
4. décision d'initier une periode d'essai
    - les habitant⋅e⋅s se conserte pour décider d'entamer une periode d'essai avec la personne candidate ou non
    - la personne candidate décide de donner suite ou non (et si possible prévien aussi si elle ne donne pas suite)

    C'est durant cette étape qu'un ajustement de la durée de la periode d'essai peut être décidée (3 mois par défaut, 1 semaine minimum) pour correspondre aux enjeux du moment et des personnes concernées (en fonction des confiances pré-existante aussi).

5. periode d'essai
    La finalité de cette periode est d'apprendre à se connaitre mutuellement dans la vie quotidienne ainsi que de vérifier que les processus existant convienne à la nouvelle personne ou en proposer des évolutions à valider collectivement sinon.
4. adoption / pérénisation
    A l'issue de la periode d'essai, une première réunion sans la/les personnes à l'essai à lieu pour décider des degrés d'adoption consentis.
    Puis s'il y a plusieurs personnes à l'essai, chacune est invitée sans la présence de l'autre à se prononcer (avec la possibilité de mettre un véto / d'expliciter une incompatibiltié mutuelle, dans quel cas il y aura de nouveau un temps de discussion et décision entre habitant⋅e⋅s qui ne sont pas en periode d'essai pour tenir compte des nouvelles info).
    Enfin, un dernier temps avec tout le monde as lieu pour annoncer les options possible, choisir avec les concerné⋅e⋅s celles qui sont choisi, et célébrer.
    
    Note : c'est aussi à l'issue de cette phase qu'il est possible de tenter auprès de l'agence une entrée officielle sur le baïl. Dans ce cas, un préavis de départ signé et non daté sera transmis au tier de confiance choisi par Labôrizon pour gérer les exclusions le cas échéant (Loïc ? Nathalie & Dieudonné ? un notaire ? David ?).
    
    Les status possibles sont : 
    - habitant⋅e (sans limite de temps / à l'année)
    - habitant⋅e à temps partiel (sans chambre dédiée, avec véto contestable tant qu'une stratégie alternative est trouvée)
    - invité⋅e récurrent⋅e
    - invité⋅e toléré⋅e (avec compensation d'hébergement systématique)
    - Non bienvenue / exclue







| Status                            | Pouvoir décisionnel | Contribution financière | Accès à l'espace |
| --------------------------------- | -------- | -------- | -------- |
| **habitant⋅e**                    | Décisionnaire, véto ferme sauf à l'exclusion | Partage égalitaire des frais (loyer et charge à minimat) | Chambre dédiée/privée + accès aux commun |
| **habitant⋅e à temps partiel**    | Décisionnaire, véto contestable mais pas ignorable | Montant mensuel fixe convenue avec les habitant⋅e⋅s | Casier/meuble privatif + accès aux commun avec couchage |
| **hébergé⋅e (dépannage/urgence)** | Consultatif | Négociée | Au cas par cas, similaire aux habitant⋅e⋅s à temps plein ou partiel |
| **invité⋅e récurrent⋅e**          | Consultatif & sans obligation d'information | Libre | Accès aux communs sur demande auprès d'un⋅e habitant⋅e (y compris à temps partiel) |
| **invité⋅e toléré⋅e**             | Consultable à la discression des habitant⋅e⋅s | 17€50/nuit | accès au cas part cas sur validation des habitant⋅e⋅s présente sur la periode concernée |
| **Non bienvenue / exclue**        | Aucun |  | Pas d'accès sauf sur invitation d'un⋅e habitant⋅e validée par l'ensemble des autres et sur une periode prédéterminée explicitement |

Colonne saisie des outils de **conflit** ?
Colonne obtention de status & perte de status ?
    
#### Processus de sortie volontaire
- 1 mois de préavis minimum (au moins coté loyer, coté présence tant qu'il reste possible de discuter à distance, c'est ok de partir plus vite)
- rendre les clefs
- si sur le baïl, demander au tier de confiance de transmettre le préavis de départ à l'agence (ou une fois le départ acté par l'agence si un autre préavis à été envoyé, un⋅e habitant⋅e poura demander au tier de confiance de détruire le préavi obsolete)
- caution ?
- affaires restantes ?
- nettoyage/remise en ordre


#### Processus d'exclusion
  Voir : https://demo.hedgedoc.org/gtRMrYQFQnqTZ7Sm3zSfCw#

#### Attendus, droits & devoirs de l'habitant.e
- Droit : véto décisionnel (sauf sur le processus d'exclusion) -> décision au consentement
- Devoir : En moyenne, participer à au moins 1 réunion sur 2, si possible plus.
- Devoir : verser 1 mois de loyer après avoir prévenu de sa décision de départ. (équivalent d'un préavis d'un mois)
Les engagements du coloc
- faire de son mieux pour participer aux réunions
- participer à la vie de la coloc (réu, journée ménage, entretien)

Annexe : 
- inspiration [code social](https://contributivecommons.org/le-code-social-une-definition/)

